import numpy as np
import cv2 as cv

num_tiles_x = 14
num_tiles_y = 12
num_labels = 6
tile_size = 64
data_file_name = "alpha_64_14_b.png"


img = cv.imread(data_file_name)
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Now we split the image to 5000 cells, each 20x20 size
cells = [np.hsplit(row, num_tiles_x) for row in np.vsplit(gray, num_tiles_y)]

# Make it into a Numpy array: its size will be (num_tiles_y, num_tiles_x, tile_size, tile_size)
x = np.array(cells)

# Now we prepare the training data and test data
half_split = len(cells[0])//2
samples_per_label = len(cells)*half_split // num_labels

# Size = (2500,400)
train = x[:, :half_split].reshape(-1, tile_size**2).astype(np.float32)
test = x[:, half_split:].reshape(-1, tile_size**2).astype(np.float32)

# Create labels for train and test data
k = np.arange(num_labels)
train_labels = np.repeat(k, samples_per_label)[:, np.newaxis]
test_labels = train_labels.copy()

# Initiate kNN, train it on the training data, then test it with the test data with k=1
knn = cv.ml.KNearest_create()
knn.train(train, cv.ml.ROW_SAMPLE, train_labels)

# Fit
K = np.arange(20)+1
A = np.array([])
for k in K:
    ret, result, neighbours, dist = knn.findNearest(test, k=k)
    matches = result == test_labels
    correct = np.count_nonzero(matches)
    accuracy = correct*100.0/result.size
    A = np.append(A, accuracy)
    print("k =", str(k).rjust(2), "- Accuracy =", "%.3f" % accuracy)

best_i = A.argmax()
print("Best k =", K[best_i], "- Accuracy =", "%.3f" % A[best_i])
