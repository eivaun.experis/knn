import cv2 as cv
import numpy as np
from sklearn.datasets import *
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split

num_tiles_x = 14
num_tiles_y = 12
num_labels = 6
tile_size = 64
data_file_name = "alpha_64_14_b.png"

# Reserve 20% of the data for testing
test_size = 0.2

img = cv.imread(data_file_name)
gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)

# Now we split the image to num_tiles_x*num_tiles_y cells, each tile_size*tile_size in size
cells = [np.hsplit(row, num_tiles_x) for row in np.vsplit(gray, num_tiles_y)]

# Make it into a Numpy array: its shape will be (num_tiles_y, num_tiles_x, tile_size, tile_size)
x = np.array(cells)

# Reshape into (num_tiles_y*num_tiles_x, tile_size*tile_size)
x = x.reshape(-1, tile_size**2).astype(np.float32)

# Generate labels
samples_per_label = x.shape[0] // num_labels
y = np.repeat(np.arange(num_labels), samples_per_label)[:, np.newaxis]
y = y.flatten()

# Split into training set and test set
x_train, x_test, y_train, y_test = train_test_split(
    x, y, test_size=test_size, random_state=2)

# Fit
K = np.arange(20)+1
A = np.array([])
for k in K:
    knn = KNeighborsClassifier(n_neighbors=k)
    knn.fit(x_train, y_train)
    accuracy = knn.score(x_test, y_test)
    A = np.append(A, accuracy)
    print("k =", str(k).rjust(2), "- Accuracy =", "%.3f" % accuracy)

best_i = A.argmax()
print("Best k =", K[best_i], "- Accuracy =", "%.3f" % A[best_i])
